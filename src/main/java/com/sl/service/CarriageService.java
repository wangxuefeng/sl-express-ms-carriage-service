package com.sl.service;

import com.baomidou.mybatisplus.extension.service.IService;
import com.sl.entity.CarriageEntity;
import com.sl.ms.carriage.domain.dto.CarriageDTO;

import java.util.List;

/**
 * 运费管理表 服务类
 */
public interface CarriageService extends IService<CarriageEntity> {

    /**
     * 获取全部运费模板
     *
     * @return 运费模板对象列表
     */
    List<CarriageDTO> findAll();
}
